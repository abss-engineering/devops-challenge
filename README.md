# DevOps Challenge

## Task
> In this task we've built a sample PHP project based on a hypothetical scenario. We'd like you to plan the release engineeing & architecture for this project.

### Scenario
The Invoicing API is a brand new service we're building to help our customers with inventory management and invoicing. It's written in PHP and uses [Laravel 5.7](https://laravel.com/docs/5.7/), plus a number of common composer dependencies.

By the time we finish this project, we know there will be a few requirements we have to meet:

 * The API will have 3-5x higher load (while most customers are using the API) between 8am and 9pm.
 * Need to be online 24/7 with at least [99.99% uptime](http://uptime.is/99.99) - we cannot afford any noticable downtime.
 * Easily deployable by development team members who should not need to manually deploy code.

### Task requirements:

In this hypothetical project, you'll be leading the release engineering. That means CI/CD, release automation (scripting & release) and infrastructure management (patching, scaling etc).

Obviously, thats a lot of work up-front, so for this task, we'd like you to:

 * Plan how you would manage all the components required for this project. You can choose the cloud/infrastructure provider, the CI/CD tools, scripting languages etc. This plan should be in the form of system architecture, approximate costings (if you're using hosted SaaS/PaaS/IaaS infrastructure) and any other information you feel is appropriate.
 * Implement **one** part of that process and show us your technical skills. For example you could choose to:
	 * Implement testing automation on your CI platform of choice, and publish the code somewhere as a build artifact.
	 * Script the release process for a zero-downtime deployment to an existing server. Note: you could use vagrant or virtualbox to simulate a 'cloud server'.
	 * Use tools like cloudformation and terraform to script the build process for infrastructure-as-code so that we can deploy it to your cloud provider of choice.
	 * Automate the building of a docker image with appropriate configuration to run in kubernetes.
	 * Any other component in the release engineering that you feel is important to your architecture/tools/code etc.

This is an open opportunity for you to show us your scripting skills around release engineering. Please feel free to choose the languages and technology you are most confident with. Ideally, we'd like you to spend no more than 2-4 hours on this task.

_Hint: We suggest cloning this repository and creating your own copy instead of forking. That ensures your copy remains private._

### Deliverables

Once completed you should have:

 * Your architecture & release engineering plan in a format of your choice. It could be a git repository with markdown and diagrams, a presentation or a document.
 * Your code sample, showing one part of the release engineering for this API. This can be a public Gitlab/Bitbucket/GitHub repository, or you can email the hiring manager (you'll have this information in your email) to organise sending a private invitation instead.

## Questions?
If you have any questions about completing this challenge, please email the hiring manager (you'll have this information in your email).


## About this project
> This is a simple API to calculate an invoice based on discounts, taxes and item prices.

### Installation
```sh
composer install
php artisan key:generate
```

_Note: This project uses a static .csv data set and does not require a database_

#### Running this API
To run this API locally, simply use PHP and Laravel's built in server:

```
php artisan serve
```

For production purposes, we recommend using Apache or Nginx instead; do not use PHP's built-in server for production workloads.

### Tests
Tests are located in `tests/` including integration/feature tests in `tests/features/`.

Once installed (see above), you can run tests using:

```
vendor/bin/phpunit tests
```
