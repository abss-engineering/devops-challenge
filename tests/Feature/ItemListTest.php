<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemListTest extends TestCase
{
    /**
      * Test /item endpoint (success)
      *
      * @return void
      */
    public function testDoesReturnItemList()
    {
        $response = $this->json('GET', '/item');

        $response->assertStatus(200);

        $response->assertJsonSchema("https://abss-engineering-php-challenge.netlify.com/schemas/item-list.json");
    }
}
