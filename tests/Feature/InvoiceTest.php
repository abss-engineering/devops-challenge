<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvoiceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $post_data = [
            'items' => [
              [
                'id' => '90192182-2f83-40cb-b324-ce4a43dd0024',
                'quantity' => 10,
                'discount' => 50,
              ],
              [
                'id' => '49721cc3-d820-41ce-880d-b854d1724a7e',
                'quantity' => 1,
                'discount' => 50,
              ],
            ],
        ];

        $response = $this->json('POST', '/invoice', $post_data);
        $response->assertStatus(200);

        $response->assertJsonSchema("https://abss-engineering-php-challenge.netlify.com/schemas/invoice.json");

        $response->assertJson([
            "tax" => 1.7,
            "total" => 39.25
        ]);
    }
}
