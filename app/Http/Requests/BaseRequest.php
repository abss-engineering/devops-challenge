<?php
namespace App\Http\Requests;
use Illuminate\Http\Request;

# Originally from HackerNoon:
# https://hackernoon.com/always-return-json-with-laravel-api-870c46c5efb2

class BaseRequest extends Request
{
    public function expectsJson()
    {
        return true;
    }
    public function wantsJson()
    {
        return true;
    }
}