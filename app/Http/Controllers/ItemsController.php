<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;


class ItemsController extends Controller
{
    /**
     * Show a list of items
     *
     * @return Response
     */
    public function index()
    {
        $items = Storage::disk('local')->get('items.csv');
        $csv = Reader::createFromString($items);
        $keys = ['id', 'name', 'tax', 'price'];
        
        $itemList = [];
        
        $records = $csv->getRecords($keys);
        foreach ($records as $offset => $record) {
            $itemList[] = [
                'id' => $record['id'],
                'name' => $record['name'],
                'tax' => (float) $record['tax'],
                'price' => round((float) $record['price'], 2),
            ];    
        }
        
        return response()->json($itemList);
    }
}
