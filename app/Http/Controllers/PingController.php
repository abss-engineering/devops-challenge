<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PingController extends Controller
{
    /**
     * A simple ping healthcheck endpoint
     *
     * @return Response
     */
    public function __invoke()
    {
        try {
            $items = Storage::disk('local')->get('items.csv');
            return response()->json([
                'ping' => 'pong'
            ]);
        } catch (\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
            // Could not load the sample data.
            return response()->json([
                'id' => '00001',
                'message' => 'Inventory unavailable.',
            ]);
        }catch (\Exception $e) {
            // Could not load the sample data.
            return response()->json([
                'id' => '99999',
                'message' => 'Unknown error',
            ]);
        }
        // Successful response is returned in the try {}.
    }
}
