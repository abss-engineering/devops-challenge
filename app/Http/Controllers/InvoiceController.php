<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;

class InvoiceController extends Controller
{
     /**
     * Calculate the invoice taxes and totals
     *
     * @param  Request  $request
     * @return Response
     */
    public function calculate(Request $request)
    {
        $invoice_items = $request->input('items');

        // Todo: Turn this into a facade or map it a model factory.
        $items_csv = Storage::disk('local')->get('items.csv');
        $csv_reader = Reader::createFromString($items_csv);
        $csv_keys = ['id', 'name', 'tax', 'price'];
        
        $inventory = [];
        
        $records = $csv_reader->getRecords($csv_keys);
        foreach ($records as $offset => $record) {
            $inventory[$record['id']] = [
                'id' => $record['id'],
                'name' => $record['name'],
                'tax' => (float) $record['tax'],
                'price' => round((float) $record['price'], 2),
            ];    
        }

        $calculated_lines = [];

        $running_ex_total = 0;
        $running_tax_total = 0;

        foreach($invoice_items as $item) {

            // Check that the requested item exists in the inventory
            if(!in_array($item['id'], array_keys($inventory))) {
                // Fail - requested item does not exist
                return response()->json([
                    'id' => '00003',
                    'message' => "Item {$item['id']} does not exist",
                ])->setStatusCode(400);
            }

            $inventory_item = $inventory[$item['id']];

            // Determine the tax calculations for this line.
            $line_taxable_total = $inventory_item['price'] * $item['quantity'] * (1 - $item['discount']/100) ;
            $line_tax_rate = ($inventory_item['tax']/100);
            $line_tax = round($line_taxable_total * $line_tax_rate, 4);
            $line_total = round($line_taxable_total + $line_tax, 2);

            // Maintain a running total for the invoice.
            $running_tax_total += $line_tax;
            $running_ex_total += $line_taxable_total;

            // Map the values we want to use for the calculated lines.
            $calculated_line = [
                'id' => $inventory_item['id'],
                'name' => $inventory_item['name'],
                'price' => $inventory_item['price'],
                'quantity' => $item['quantity'],
                'discount' => $item['discount'],
                'tax_rate' => $inventory_item['tax'],
                'total_ex' => $line_taxable_total,
                'tax_value' => $line_tax,
                'total' => $line_total,
            ];
            
            $calculated_lines[] = $calculated_line;
        }

        $tax_total = round($running_tax_total, 2);
        
        // Determine the final JSON response including formatting.
        $invoice_lines = [];
        foreach($calculated_lines as $invoice_line) {
            $invoice_lines[] = [
                'id' => $invoice_line['id'],
                'name' => $invoice_line['name'],
                'quantity' => $invoice_line['quantity'],
                'discount' => $invoice_line['discount'], // x%
                'tax_rate' => $invoice_line['tax_rate'], // x%
                'total' => $invoice_line['total'], // tax incl.
            ];
        }

        $invoice = [
            "lines"=> $invoice_lines,
            "tax" => $tax_total,
            "total" => $running_ex_total + $tax_total,
        ];
        return response()->json($invoice);
    }
}
